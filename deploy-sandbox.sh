if [ ! -d "./data" ];then
mkdir -p data/mysql
mkdir -p data/redis
fi

if [ ! -d "./data/mysql" ];then
mkdir -p data/mysql
fi

if [ ! -d "./data/redis" ];then
mkdir -p data/redis
fi

docker stack deploy --compose-file=docker-stack.development.yml ${PWD##*/}
