set -e

cd ../../fendx/
# clean old
rm -rf app bin logs www fendx

# cp new
cp -r ../fendx-demo/.gitignore .gitignore
cp -r ../fendx-demo/app app
cp -r ../fendx-demo/bin bin
mkdir logs
cp -r ../fendx-demo/www www

#init fendx
mkdir fendx
cp -r ../fendx-demo/init.php init.php

cp -r ../fendx-demo/vendor/php/fendx-core/src/* fendx/

cp -r ../fendx-demo/vendor/php/fendx-plugin-cache/src/* fendx/

cp -r ../fendx-demo/vendor/php/fendx-plugin-db/src/* fendx/

mkdir fendx/Router
cp -r ../fendx-demo/vendor/php/fendx-plugin-fastrouter/src/* fendx/Router/

cp -r ../fendx-demo/vendor/php/fendx-plugin-kafka/src/* fendx/

mkdir fendx/Cache
cp -r ../fendx-demo/vendor/php/fendx-plugin-memcache/src/* fendx/Cache/

cp -r ../fendx-demo/vendor/php/fendx-plugin-mysqli/src/* fendx/Db/
cp -r ../fendx-demo/vendor/php/fendx-plugin-pdo/src/* fendx/Db/
cp -r ../fendx-demo/vendor/php/fendx-plugin-queue/src/* fendx/
cp -r ../fendx-demo/vendor/php/fendx-plugin-rabbitmq/src/* fendx/
cp -r ../fendx-demo/vendor/php/fendx-plugin-redis/src/* fendx/
cp -r ../fendx-demo/vendor/php/fendx-plugin-redismodel/src/* fendx/Redis
cp -r ../fendx-demo/vendor/php/fendx-plugin-router/src/* fendx/Router

mkdir fendx/Server
cp -r ../fendx-demo/vendor/php/fendx-plugin-server/src/* fendx/Server
cp -r ../fendx-demo/vendor/php/fendx-plugin-validator/src/* fendx/

cd ../fendx-demo/bin
