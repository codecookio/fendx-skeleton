<?php

use Fendx\Config;
use Fendx\Log\EagleEye;
use Fendx\Log\LogAgent;
use Fendx\Logger;
use Fendx\Funcs\FendxUtil;
use Fendx\Component\Component;

//ini_set('display_errors', 'on');//开启或关闭PHP异常信息
date_default_timezone_set('Asia/Shanghai');
error_reporting(E_ALL);//异常级别设置

//系统设置不可替换的静态配置
define('FD_DS', DIRECTORY_SEPARATOR);//定制目录符合
define('SYS_ROOTDIR', dirname(__FILE__) . FD_DS);

define('SRC_ROOTDIR', SYS_ROOTDIR . 'src' . FD_DS);

define('APP_PATH', SYS_ROOTDIR . 'src' . FD_DS . 'app' . FD_DS); // APP路径

define('ETC_PATH', SYS_ROOTDIR . 'src' . FD_DS . 'etc' . FD_DS); // APP路径

define('SYS_CACHE', SYS_ROOTDIR . 'cache' . FD_DS);//cache目录，smarty cache，文件缓存放置区域

define('SYS_VIEW', SYS_ROOTDIR . 'app' . FD_DS . 'View' . FD_DS);//http业务模板层

define('ERR_CODE_PATH', APP_PATH . '%s' . FD_DS . 'Const' . FD_DS); // 错误码定义路径

define('DATABASE_PATH',  '%s' . FD_DS . 'Database' . FD_DS); // 数据库路径

define('EXEC_PATH',  '%s' . FD_DS . 'Exec' . FD_DS); // 数据库路径
//autoload not init tips
//加载autoload
if(!include_once(SYS_ROOTDIR. 'vendor/autoload.php')){
    echo "please run composer update. on project root to init.\n";
    exit;
}


//设置配置，加载路径
\Fendx\Config::setConfigPath(SYS_ROOTDIR . 'src/etc');
\Fendx\Config::set('fendx_err_code_file', ETC_PATH  . 'ErrCode.php');

//开启根据环境加载指定目录配置
\Fendx\Env::load(SYS_ROOTDIR);

//初始化日志
$fendx = Config::get("Fendx");

EagleEye::disable(!$fendx["log"]["trace"]);
LogAgent::setLogPath($fendx["log"]["path"]);
Logger::setLogLevel($fendx["log"]["level"]);
LogAgent::setFormat($fendx["log"]["logFormat"] ?? "json");
LogAgent::setLogRoll($fendx["log"]["logRoll"] ?? "day");
LogAgent::setFileNameWithPid($fendx["log"]["filenameWithPid"] ?? true);
LogAgent::setLogPrefix($fendx["log"]["logPrefix"] ?? "");

// 加载组件
$registrations = Config::get("Registrations");
FendxUtil::loadFiles($registrations);
Component::loadInit();
