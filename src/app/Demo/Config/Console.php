<?php
declare(strict_types=1);

return [
    'command_namespace' => "\\App\Demo\Exec\\Command\\",
    'command_path'  => __DIR__ . '/../Exec/Command',
];
