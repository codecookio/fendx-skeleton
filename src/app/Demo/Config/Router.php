<?php
//新路由解析
////////////////////////////////////////////////////////////
//注意：新路由修改后,记得清空cache/router内缓存文件
////////////////////////////////////////////////////////////

return [
    'tal' => [
        'prefix' => '/rest/2.0/tal/',
        'root' => '\\App\\Demo\\Http\\Tal',
        'middlewares' => [
            // 中间件功能
            \Fendx\MiddleWare\CorsMiddleware::class,
            \Fendx\MiddleWare\ApiRendererMiddleware::class,
        ],
        'options' => ['sign', 'jwt'],
    ],
//    'admin' => [
//        'prefix' => '/rest/2.0/mall',
//        'root' => "\\App\\Http\\Tal", //namespace
//        'domain' => 'admin.lichao.com',
//        'middlewares' => [
//            // 中间件功能
//            \App\MiddleWare\CorsMiddleware::class,
//            \App\MiddleWare\ApiRendererMiddleware::class
//        ],
//        'options' => ['sign', ''],
//    ]
];
