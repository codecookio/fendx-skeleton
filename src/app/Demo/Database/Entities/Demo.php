<?php
namespace App\Demo\Database\Entities;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;
use Doctrine\ORM\MAPPING\UniqueConstraint;


/**
 * demo
 *
 * @ORM\Entity(repositoryClass="\App\Demo\Database\Mysql\DemoRepo")
 * @ORM\Table(
 *     name="demo"
 * )
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE")
 */
class Demo {

    /**
     * @var integer
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * 用户ID
     *
     * @ORM\Column(name="user_id", type="integer", unique=true)
     */
    private int $user_id;

    /**
     * 用户名
     *
     * @ORM\Column(name="username", type="string", length=64)
     */
    private string $username;

    /**
     * 备注
     *
     * @ORM\Column(name="comment", type="string", length=512)
     */
    private string $comment;

    /**
     * 扩展字段
     *
     * @ORM\Column(name="default_region", type="json")
     */
    private array $extras = [];

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->user_id;
    }

    /**
     * @param int $user_id
     */
    public function setUserId(int $user_id): void
    {
        $this->user_id = $user_id;
    }
}
