<?php
//新路由解析
////////////////////////////////////////////////////////////
//注意：新路由修改后,记得清空cache/router内缓存文件
////////////////////////////////////////////////////////////

return [
    'rest' => [
        'prefix' => '/rest/2.0/main/',
        'root' => '\\App\\Main\\Http',
        'middlewares' => [
            // 中间件功能
            \Fendx\MiddleWare\CorsMiddleware::class,
            \Fendx\MiddleWare\ApiRendererMiddleware::class,
           // \Fendx\MiddleWare\AuthorizationMiddleware::class,
        ],
        'options' => ['oauth=admin'],
    ],
];
