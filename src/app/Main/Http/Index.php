<?php
namespace App\Main\Http;

use Fendx\Annotation\GetMapping;
use Fendx\Annotation\PutMapping;
use Fendx\Annotation\Controller;
use Fendx\Annotation\RequestMapping;
use Fendx\Exception\BizException;
use Fendx\Exception\ErrCode;
use Fendx\Request;
use Fendx\Response;
use Fendx\ValidationRequest;

/**
 * Class Index
 * 首页
 * @author gary
 *
 * @Controller(prefix="index")
 */
class Index
{

    /**
     * GET
     *
     * @RequestMapping(path="/get_and_post/{id:\d+}", methods={"get", "post"}, options={"sign"})
     */
    public function get(Request $request, Response $response)
    {
        return [
            'method' => 'GET'
        ];
    }

    /**
     * GET
     *
     * @RequestMapping(path="/test/{id:\d+}", methods={"get", "post"})
     */
    public function test(Request $request, Response $response)
    {
        $params = ValidationRequest::make($request, [
            'id' => ['param', 'abc', 'int', 'require'],
            'getInt1' => ['get', 0, 'int', 'require|range:10,100'],
            'getString1' => ['get', "default1", 'string', 'require|length:2,10'],
            // 'jsonInt1' => ['json', 0, 'int', 'require|range:10,100'],
            // 'jsonString1' => ['json', "default1", 'string', 'require|length:2,10'],
            'postInt1' => ['post', 0, 'int', 'require|range:10,100'],
            'postString1' => ['post', "default1", 'string', 'require|length:2,10'],
        ]);
        return [
            'method' => $request->getMethod(),
            "params" => $params,
        ];
    }

    /**
     * POST
     *
     * @GetMapping(path="/post", options={"sign"})
     */
    public function post(Request $request, Response $response)
    {
        return [
            'method' => 'POST'
        ];
    }

    /**
     * PUT
     *
     * @PutMapping(path="/put", options={"sign"})
     */
    public function put(Request $request, Response $response)
    {
        return [
            'method' => 'PUT'
        ];
    }

    /**
     * DELETE
     *
     * @PutMapping(path="/delete", options={"sign"})
     */
    public function delete(Request $request, Response $response)
    {
        return [
            'method' => 'DELETE'
        ];
    }
}
