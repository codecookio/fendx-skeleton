<?php
namespace App\Main\Exception\Handler;

use Fendx\Di;
use Fendx\ExceptionHandle\ExceptionHandleInterface;
use Fendx\Log\EagleEye;
use Fendx\Logger;

class AppExceptionHandler implements ExceptionHandleInterface
{

    /**
     * @param \Throwable $e 异常
     * @param string $result 返回值
     *
     * @return string result结果
     * @throws \Throwable
     */
    public static function handle(\Throwable $e, string $result): string
    {
        $request = Di::factory()->getRequest();
        $response = Di::factory()->getResponse();

        $response->status(500);

        //record exception log
        Logger::exception(basename($e->getFile()), $e->getFile(), $e->getLine(), $e->getMessage(), $e->getCode(), $e->getTraceAsString(), array(
            "action"    => $request->header("HOST") . $request->server("REQUEST_URI"),
            "server_ip" => gethostname(),
        ));

        if (EagleEye::isEnable()) {
            EagleEye::setMultiRequestLogInfo([
                "code" => 500,
                "backtrace" => $e->getMessage() . "\r\n" . $e->getTraceAsString()
            ]);
        }

        return $result;
    }
}
