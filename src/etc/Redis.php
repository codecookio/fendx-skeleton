<?php
/**
 * Redis配置
 */

return array(
    'default' => ['host' => '127.0.0.1', 'port' => 6379, 'pre' => 'ts_', 'pwd' => ''],
    'fendx_test' => ['host' => '127.0.0.1', 'port' => 6379, 'pre' => 'f_', 'pwd' => '', 'db'=> 1],
    'fendx_test_broken' => ['host' => '227.0.0.1', 'port' => 6379, 'pre' => 'f_', 'pwd' => ''],
    'fendx_test_pwd' => ['host' => '127.0.0.1', 'port' => 6379, 'pre' => 'f_', 'pwd' => '123'],
    'key_map_path' => SYS_ROOTDIR . 'tests/app/Config/RedisKey',

);
